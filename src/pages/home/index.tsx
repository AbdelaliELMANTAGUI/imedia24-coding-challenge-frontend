import "./home.css"
import PokemonCard from "../../components/card"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from '../../redux/store/pokemonStore'
import Pokemon from "../../types/pokemon"
import Header from "../../components/header"
import InfiniteScroll from "react-infinite-scroll-component"
import { loadPokemon } from "../../redux/slice/pokemonSlice"
import Loader from "../../components/loader"
import { FETCH_SIZE } from "../../constants/fetchConstants"
import Modal from "../../components/modal"
import ModalCard from "../../components/ModalCard"
import { useState } from "react"

function HomePage() {
    const pokemon = useSelector((state: RootState) => state.pokemon.pokemon)
    const dispatch = useDispatch()
    const [selectedPokemon, setSelectedPokemon] = useState<Pokemon|null>(null)
    const isLoading = useSelector((state: RootState) => state.pokemon.isLoading)
    const total = useSelector((state: RootState) => state.pokemon.total)
    const page = useSelector((state: RootState) => state.pokemon.page)
    const hasMorePokemon = total != null ? page * FETCH_SIZE < total : true    

    const loadData = () => dispatch(loadPokemon())

    return <>
        <div className="w-full">
            <Header />
            {selectedPokemon &&
                <Modal
                    isOpen={selectedPokemon != null}
                    onClose={()=> {                        
                        setSelectedPokemon(null)
                    }}
                >
                    <ModalCard pokemon={selectedPokemon} />
                </Modal>
            }
            <div className="w-full">
                <InfiniteScroll
                    className="w-full grid gap-4 grid-cols-1 md:grid-cols-3 lg:grid-cols-4 container mx-auto mb-4"
                    dataLength={pokemon.length}
                    scrollThreshold={.99}
                    next={loadData}
                    hasMore={hasMorePokemon}
                    loader={(<></>)}
                    endMessage={<p>No more Pokemon to see.</p>}
                >
                    {
                        pokemon.map((poke: Pokemon) => {
                            return (
                                <PokemonCard
                                    key={poke.id}
                                    image={poke.officialArtwork}
                                    name={poke.name}    
                                    selectHandler={()=> {                                        
                                        setSelectedPokemon(poke)}}                            
                                    stats={{
                                        hp: poke.stats.hp,
                                        attack: poke.stats.attack,
                                        defense: poke.stats.defense,
                                    }} />
                            )
                        })
                    }

                </InfiniteScroll>
                {isLoading && <div className="w-full flex justify-center mb-4">
                    <Loader />
                </div>
                }
            </div>
        </div>
    </>
}

export default HomePage