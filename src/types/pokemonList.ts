interface PokemonResult {
    name: string;
    url: string
}

export default interface PokemonList {
    count: number;
    results: PokemonResult[]
}