interface PokemonStats {
    hp: number;
    attack: number;
    defense: number;
    specialAttack: number,
    specialDefense: number,
    speed: number,
}

export default interface Pokemon {
    id: number;
    name: string;
    officialArtwork: string;
    stats: PokemonStats;
    weight: number;
    height: number;
}