import { configureStore } from '@reduxjs/toolkit'
import pokemonSlice, { loadPokemon } from '../slice/pokemonSlice'
import createSagaMiddleware from 'redux-saga'
import mainSaga from '../saga/pokemonSaga'



const sagaMiddleware = createSagaMiddleware()

export const store = configureStore({
  reducer: {
    pokemon: pokemonSlice
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware),
})

sagaMiddleware.run(mainSaga)

store.dispatch(loadPokemon())

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch