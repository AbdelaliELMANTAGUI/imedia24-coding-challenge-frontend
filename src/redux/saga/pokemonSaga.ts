import { all, call, fork, put, select, takeEvery } from "redux-saga/effects";
import { LOAD_POKEMON_ACTION } from "../../constants/actionsContants";
import getPokemonList from "../../api/getPokemonList";
import PokemonList from "../../types/pokemonList";
import { addPokemon, nextPage, setPokemonIsLoading, setPokemonTotal } from "../slice/pokemonSlice";
import getPokemonDetails from "../../api/getPokemonDetails";
import Pokemon from "../../types/pokemon";

function* fetchPokemonDetails(url: string){    
    const pokemon: Pokemon = yield call(getPokemonDetails, url)    
    yield put(addPokemon(pokemon))
}

function* fetchPokemonList(){
    yield put(setPokemonIsLoading(true))
    const page: number = yield select(state => state.pokemon.page)
    const pokemonList:PokemonList = yield call(getPokemonList, page)
    yield put(setPokemonTotal(pokemonList.count))    
    const forks = pokemonList.results.map(pokemon => fork(fetchPokemonDetails, pokemon.url))
    yield all(forks)
    yield put(setPokemonIsLoading(false))
    yield put(nextPage())    
}


export default function* mainSaga(){
    yield takeEvery(LOAD_POKEMON_ACTION, fetchPokemonList)
}