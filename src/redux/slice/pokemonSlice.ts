import { createAction, createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import Pokemon from '../../types/pokemon';
import { LOAD_POKEMON_ACTION } from '../../constants/actionsContants';


export interface PokemonState {
  pokemon: Pokemon[];
  page: number;
  total: number | null;
  isLoading: boolean;
  error: string | null
}

const initialState: PokemonState = {
    pokemon: [],
    page: 1,
    total: null,
    isLoading: false,
    error: null
}

export const pokemonSlice = createSlice({
  name: 'pokemon',
  initialState,
  reducers: {
    addPokemon: (state, action) => {
      state.pokemon = [...state.pokemon, action.payload]
    },
    setPokemonIsLoading: (state, action) => {
      state.isLoading = action.payload
    },
    setPokemonError: (state, action: PayloadAction<string | null>) => {
      state.error = action.payload
    },
    setPokemonTotal: (state, action) => {
        state.total = action.payload
      },
    nextPage(state){
        state.page += 1
    }
  },
})


export const { addPokemon, setPokemonIsLoading, setPokemonTotal, setPokemonError, nextPage } = pokemonSlice.actions

export const loadPokemon = createAction(LOAD_POKEMON_ACTION)

export default pokemonSlice.reducer