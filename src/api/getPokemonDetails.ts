import Pokemon from "../types/pokemon"

function mapToPokemonDetails(body: any): Pokemon {
    return {
        id: body.id,
        name: body.name,
        officialArtwork: body.sprites.other['official-artwork'].front_default,
        stats: {
            hp: body.stats.find((s: any) => s.stat.name == "hp").base_stat,
            attack: body.stats.find((s: any) => s.stat.name == "attack").base_stat,
            defense: body.stats.find((s: any) => s.stat.name == "defense").base_stat,
            specialAttack: body.stats.find((s: any) => s.stat.name == "special-attack").base_stat,
            specialDefense: body.stats.find((s: any) => s.stat.name == "special-defense").base_stat,
            speed: body.stats.find((s: any) => s.stat.name == "speed").base_stat,
        },
        weight: body.weight,
        height: body.height
    }
}

export default async function getPokemonDetails(url: string): Promise<Pokemon> {
    const res = await fetch(url)
    const body = await res.json()
    return mapToPokemonDetails(body)
}