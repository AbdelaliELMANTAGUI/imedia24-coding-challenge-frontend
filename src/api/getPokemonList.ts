import { FETCH_SIZE } from "../constants/fetchConstants"
import PokemonList from "../types/pokemonList"

const size = FETCH_SIZE
const url = "https://pokeapi.co/api/v2/pokemon"
export default async function getPokemonList(page: number): Promise<PokemonList>{
    const params = `?limit=${size}&offset=${size * (page - 1)}`
    const fullUrl = url + params
    const res = await fetch(fullUrl)
    const body = await res.json()
    return body
}