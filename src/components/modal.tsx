import { useRef, useEffect, useState, KeyboardEventHandler } from 'react';
import CloseIcon from '../assets/close.png'


interface ModalProps {
    isOpen: boolean;
    children: any;
    onClose: () => void
}

const Modal = ({ isOpen, onClose, children }: ModalProps) => {
  const [isModalOpen, setModalOpen] = useState(isOpen);
  const modalRef = useRef<HTMLDialogElement>(null);
    
  const handleCloseModal = () => {
    if(onClose){        
        onClose()
    }
    setModalOpen(false);
  };

  const handleKeyDown:KeyboardEventHandler<HTMLDialogElement> = () => {
      handleCloseModal();    
  };

  useEffect(() => {
    setModalOpen(isOpen);
  }, [isOpen]);

  useEffect(() => {
    const modalElement = modalRef.current;
    if (modalElement) {
      if (isModalOpen && !modalElement.open) {
        modalElement.showModal();
      } 
      if ( !isModalOpen && modalElement.open ) {
        modalElement.close();
      }
    }
  }, [isModalOpen]);

  return (
    <dialog ref={modalRef} onClose={handleKeyDown} className="backdrop:bg-sky-800 backdrop:opacity-30 mx-auto my-auto rounded-lg">
        <div className='w-full flex justify-end p-2'>
            <img onClick={() => handleCloseModal()} className='w-6 cursor-pointer' src={CloseIcon} alt="close icon" />
        </div>
      {children}
    </dialog>
  );
};

export default Modal;
