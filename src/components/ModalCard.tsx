import Pokemon from "../types/pokemon";

export default function ModalCard({pokemon}: {pokemon: Pokemon}) {
    return (
        <div className="w-80 md:w-96 flex flex-col p-4">
            <img className="w-11/12" src={pokemon.officialArtwork} alt="pokemon image" />
            <h1 className="w-full text-4xl text-center mt-2">{pokemon.name}</h1>
            <div className="flex flex-row justify-around mt-2">
                <h2>Weight: { pokemon.weight } g</h2>
                <h2>Height: { pokemon.height } Cm</h2>
            </div>
            <h2 className="text-lg mt-2">Normal</h2>
            <div className="w-full grid gap-4 grid-cols-3 ">
                <div className="flex flex-col items-center">
                    <span className="text-3xl">{pokemon.stats.hp}</span>
                    <span>Hp</span>
                </div>
                <div className="flex flex-col items-center">
                    <span className="text-3xl">{pokemon.stats.attack}</span>
                    <span>Attack</span>
                </div>
                <div className="flex flex-col items-center">
                    <span className="text-3xl">{pokemon.stats.defense}</span>
                    <span>Defense</span>
                </div>
            </div>
            <h2 className="text-lg  mt-1">Special</h2>
            <div className="w-full grid gap-4 grid-cols-3">
                <div className="flex flex-col items-center justify-center">
                    <span className="text-3xl">{pokemon.stats.specialAttack}</span>
                    <span>Attack</span>
                </div>
                <div className="flex flex-col items-center justify-center">
                    <span className="text-3xl">{pokemon.stats.specialDefense}</span>
                    <span>Defence</span>
                </div>
                <div className="flex flex-col items-center justify-center">
                    <span className="text-3xl">{pokemon.stats.speed}</span>
                    <span>Speed</span>
                </div>
            </div>
            <h2></h2>
        </div>
    )
}