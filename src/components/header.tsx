import Pokeball from "../assets/Pokeball.png"

export default function Header(){
    return (<div className="w-full h-80 bg-blue-900 flex justify-center items-center">
    <div className="flex items-center">
        <span className="md:text-7xl text-4xl text-zinc-300">&lt;Pokemon</span> 
        <img className="md:w-28 w-20" src={Pokeball} alt="pokemon ball" />
        <span className="md:text-7xl text-4xl text-zinc-300">/&gt;</span> 
    </div>
</div>)
}