function Card({image, name, stats, selectHandler}:CardProps){
    return (<div className="pokemone-card w-full relative mt-40 justify-self-stretch flex flex-col items-center p-2 bg-sky-600 rounded-md cursor-pointer"
        onClick={selectHandler}
    >
    <div className="w-10/12 h-60 absolute -top-36 flex justify-center">
        <img className="w-10/12 h-11/12	" src={image} alt="" />
    </div>
    <div className="w-full mt-24 text-zinc-100 flex-col items-center">
        <h1 className="text-xl h-8 text-center font-extrabold">{name}</h1>
        <div className="flex flex-row gap0 justify-around">
            <div className="flex flex-col items-center"> 
                <span className="text-md text-gray-200">HP</span> 
                <span className="text-xl font-bold">{stats.hp}</span>
            </div>
            <div className="flex flex-col items-center"> 
                <span className="text-md text-gray-200">ATTACK</span> 
                <span className="text-xl font-bold">{stats.attack}</span>
            </div>
            <div className="flex flex-col items-center"> 
                <span className="text-md text-gray-200">DEFENSE</span> 
                <span className="text-xl font-bold">{stats.defense}</span>
            </div>
        </div>
    </div>
</div>)
}

type PokemonStats = {
    hp: number;
    attack: number;
    defense: number;
}

type CardProps = {
    image: string;
    name: string;
    stats: PokemonStats;
    selectHandler: () => void
}

export default Card