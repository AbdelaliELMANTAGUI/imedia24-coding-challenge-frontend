const SIZE = 20

describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:5173/')
    cy.scrollTo('bottom')
    cy.get(".pokemone-card").should('have.length.greaterThan', SIZE)    
  })
})